const mysql = require('mysql');
const format = require("string-template");
const fs = require('fs');
const ora = require('ora');
const writeFile = require('write');
const dateFormat = require('dateformat');
const XML_DEBIT_TEMPLATE = fs.readFileSync("./xml_template/debit.xml", "utf8");
const XML_CREDIT_TEMPLATE = fs.readFileSync("./xml_template/credit.xml", "utf8");
const XML_REFUND_TEMPLATE = fs.readFileSync("./xml_template/refund.xml", "utf8");

var ALL_XML = "<?xml version='1.0' standalone='yes'?><MovementIntegration>";

function ledger_date(date){
    // 04-Jan-2018
    var dt = new Date(date);
    return dateFormat(dt, "mm-dd-yyyy");
}

function record_num_date(date){
    // 04012018
    var dt = new Date(date);
    return dateFormat(dt, "mmddyyyy");
}

function match_ref_date(date){
    // 20180104
    var dt = new Date(date);
    return dateFormat(dt, "yyyymmdd");
}

function start(){
    const spinner = ora('Collecting data ...').start();
    var connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '{{Your Password Goes Here}}',
        database : 'shamo',
        multipleStatements: true
    });

    var QUERY = "SELECT * from sales;";
    connection.query(QUERY, function (error, results, fields) {
        // The processing happens here
        var table = results;
        for (let index = 0; index < results.length; index++) {
            const row = results[index];
            var xml_row = "";
            var data_item = JSON.parse(row.data);
            if(data_item.refunddata === undefined){ // Refund data available
                xml_row = format(XML_CREDIT_TEMPLATE, {
                    TRANS_DATE: ledger_date(data_item.dt), // maybe some filters applied here
                    AMOUNT: data_item.total,
                    DESCRIPTION: "REFUND",
                    DATE: record_num_date(data_item.dt),
                    DATE_REV: match_ref_date(data_item.dt) 
                });
            }
            else {
                xml_row = format(XML_DEBIT_TEMPLATE, {
                    TRANS_DATE: ledger_date(data_item.dt), // maybe some filters applied here
                    AMOUNT: data_item.total,
                    DESCRIPTION: "FEES PAID FOR ITEM",
                    DATE: record_num_date(data_item.dt),
                    DATE_REV: match_ref_date(data_item.dt) 
                });
            }
            ALL_XML+=xml_row;
        }
        ALL_XML+="</MovementIntegration>";
        writeFile('output/open_this.xml', ALL_XML)
        .then(function() {
            spinner.succeed("Script Completed! Check 'output' folder.");
        });
        
    });
    connection.end();
}
start();