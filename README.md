# SSG Bank XML Script
  
## New Features
  - Load Sales from Database
  - Access $ref JSON object
  - Create xml file in "output" folder


#### How to run
For production release:
```sh
$ npm install
```
Run the script:
```sh
$ npm start
```
